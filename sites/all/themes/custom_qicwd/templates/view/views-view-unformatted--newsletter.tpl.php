
	<?php if (!empty($title)): ?>
			<h2 class="h3"><?php print $title; ?></h2>
	<?php endif; ?>
	<ul<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
		<?php foreach ($rows as $id => $row): ?>
			<?php print $row; ?>
		<?php endforeach; ?>
	</ul>
</div>