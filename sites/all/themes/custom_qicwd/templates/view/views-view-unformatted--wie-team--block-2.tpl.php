<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 *
 *for WIE Team view - views for view block display
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="flex__container team-member__container">
	<?php foreach ($rows as $id => $row): ?>
	  <div class="flex__item team-member <?php if ($classes_array[$id]) { print $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </div>
	<?php endforeach; ?>
</div>