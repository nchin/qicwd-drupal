<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="flex__container updates__container updates--teaser">
	<?php foreach ($rows as $id => $row): ?>
	  <div class="flex__item updates <?php if ($classes_array[$id]) { print $classes_array[$id] .'"';  } ?>>
	    <?php print $row; ?>
	  </div>
	<?php endforeach; ?>
</div>