<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

  <div class="flex__container team-member__container">
    <?php
       print render($content['field_site_wie_team_member']);
    ?>
  </div>