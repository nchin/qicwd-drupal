<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php print render($content['field_image']); ?>

<div class="team-member__name first last">
  <a href="<?php print $node_url; ?>"><?php print $node-> field_first_name['und'][0]['value']?> <?php print $title; ?></a>
</div>
