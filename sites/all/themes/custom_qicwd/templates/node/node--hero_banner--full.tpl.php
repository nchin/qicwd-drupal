<?php

/**
 * @file
 * Radix theme implementation to display a hero banner node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="o-full-width">
  <div class="c-hero-banner">

    <div class="c-hero-banner__content">
      <div class="c-container">
        <h2 class="c-hero__title"><?php print $title; ?></h2>
        <?php
        // We hide the banner image now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_banner_image']);
        print render($content);
        ?>
      </div>
    </div>
    <div class="c-hero-banner__image">
        <?php print render($content['field_banner_image']); ?>
    </div>

  </div>
</div>

