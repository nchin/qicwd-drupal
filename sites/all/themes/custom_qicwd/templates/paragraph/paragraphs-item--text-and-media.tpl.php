<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $field_html_class = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_html_class');
  $field_section_title = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_section_title');
  $field_video = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_video');
  $field_iframe = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_iframe');
  $field_number_of_column = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_number_of_column');
  $field_number_of_columns = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_number_of_columns');
  $field_media_position = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_media_position');
  $field_call_to_action_button = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_call_to_action_button');
?>

<div class="<?php print $classes; ?> full-width <?php print $field_html_class[0]['value']; ?>" <?php print $attributes; ?>>
  <div class="content" <?php print $content_attributes; ?>>
    <div class="content__column-container <?php print $field_number_of_column[0]['value']; ?> <?php print $field_media_position[0]['value']; ?>">
      <div class="content__column content__text">
        <?php if (!empty($content['field_section_title'])): ?>
          <h2><?php print $field_section_title[0]['value']; ?></h2>
        <?php endif; ?>
        <?php if (!empty($content['field_paragraph'])): ?>
          <?php print render($content['field_paragraph']); ?>
        <?php endif; ?>
        <?php if (!empty($content['field_call_to_action_button'])): ?>
          <p>
            <a class="btn btn-primary" href="<?php print $field_call_to_action_button[0]['url']; ?>">
              <?php print $field_call_to_action_button[0]['title']; ?>
            </a>
          </p>
        <?php endif; ?>
      </div>
      <div class="content__column content__media">
        <?php print render($content['field_image']); ?>
        <?php if (!empty($content['field_video'])): ?>
            <?php print render($content['field_video']); ?>
        <?php endif; ?>
        <?php if (!empty($content['field_iframe'])): ?>
          <div class="embed-responsive embed-responsive-16by9">
            <?php print $field_iframe[0]['value']; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
