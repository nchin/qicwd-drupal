<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $field_html_class = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_html_class');
  $field_citation_source_name = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_citation_source_name');

?>

<div class="<?php print $classes; ?> full-width <?php print $field_html_class[0]['value']; ?>" <?php print $attributes; ?>>
  <div class="content" <?php print $content_attributes; ?>>

    <blockquote>
      <?php if (!empty($content['field_paragraph'])): ?>
        <?php print render($content['field_paragraph']); ?>
      <?php endif; ?>

      <?php if (!empty($content['field_citation_source_name'])): ?>
      <footer>

        <cite title="Source Title"><?php print render($content['field_citation_source_name']); ?></cite>
      </footer>
      <?php endif; ?>

  </div>
</div>