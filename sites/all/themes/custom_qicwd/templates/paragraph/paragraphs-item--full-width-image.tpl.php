<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $field_html_class = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_html_class');
  $field_section_title = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_section_title');
  $field_file_upload = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_file_upload');
  $field_call_to_action_button = field_get_items('paragraphs_item', $variables['paragraphs_item'], 'field_call_to_action_button');
?>

<div class="<?php print $classes; ?> full-width <?php print $field_html_class[0]['value']; ?>" <?php print $attributes; ?>>
  <div class="content hero__container"<?php print $content_attributes; ?>>
      <div class="hero__text">
        <?php if (!empty($content['field_section_title'])): ?>
          <h2><?php print $field_section_title[0]['value']; ?></h2>
        <?php endif; ?>
        <?php if (!empty($content['field_paragraph'])): ?>
       		<?php print render($content['field_paragraph']); ?>
       	<?php endif; ?>
        <?php if (!empty($content['field_call_to_action_button'])): ?>
          <p>
            <a class="btn btn-primary" href="<?php print $field_call_to_action_button[0]['url']; ?>">
              <?php print $field_call_to_action_button[0]['title']; ?>
            </a>
          </p>
        <?php endif; ?>
      </div>
      <div class="hero__image">
        <?php print render($content['field_image']); ?>
      </div>
      <div class="hero__overlay"></div>
    </div>
  </div>
</div>
