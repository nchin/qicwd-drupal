<footer id="footer--brand" class="footer" role="footer">
  <div class="inner-wrapper">
    <div class="row">
      <?php if ($copyright): ?>
        <div class="col-sm-9">
          <div class="copyright pull-left"><?php print $copyright; ?></div>
        </div>
      <?php endif; ?>
      <div class="col-sm-3">
        <div class="pull-right"><p><a href="#"><?php print t('Back to Top'); ?></a></p></div>
      </div>
    </div>
    <div id="footer__after">
      <?php print render($page['footer_after']); ?>
    </div>
  </div>
</footer>