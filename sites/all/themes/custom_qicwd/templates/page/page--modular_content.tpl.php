<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>

<?php require(drupal_get_path('theme', 'custom_qicwd') . '/templates/page/header.tpl.php'); ?>
  
  <main id="main" class="main">
    
    <div class="inner-wrapper">
      <?php if ($title): ?>
          <h1 class="page-title <?php if (drupal_is_front_page()): ?>sr-only<?php endif; ?>"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php if ($tabs): ?>
        <div id="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>
    </div>
    
    <?php print render($page['content_last']); ?>

  </main> <!-- /#main -->

<?php require(drupal_get_path('theme', 'custom_qicwd') . '/templates/page/footer.tpl.php'); ?>


