<?php if ($messages): ?>
  <div id="messages">
    <?php print $messages; ?>
  </div>
<?php endif; ?>

<header id="header" class="header" role="header">
    <nav id="main-navbar" class="navbar navbar-default" role="navigation">
      <div class="main-navbar__inner-wrapper">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header main-navbar__component main-navbar__left inner-wrapper">
          <div class="navbar-header__component navbar-header--left">
            <?php if ($site_name || $logo): ?>
              <a href="<?php print $front_page; ?>" class="logo__wrapper" rel="home">
                <?php if ($logo): ?>
                  <img src="<?php print $logo; ?>" alt="<?php print $site_name.t(' - Home'); ?>" id="logo" />
                <?php endif; ?>
                <?php if ($site_name): ?>
                  <span class="site-name sr-only"><?php print $site_name; ?></span>
                <?php endif; ?>
              </a>
            <?php endif; ?>
          </div><!-- /.navbar-header__component -->
          <div class="navbar-header__component navbar-header--right">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
              <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div><!-- /.navbar-header__component -->
        </div> <!-- /.navbar-header -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse main-navbar__component main-navbar__right" id="navbar-collapse">
          <div class="inner-wrapper">
            <?php if ($main_menu): ?>
              <ul id="main-menu" class="menu nav navbar-nav">
                <?php print render($main_menu); ?>
              </ul>
            <?php endif; ?>
            <?php if ($search_form): ?>
              <?php print $search_form; ?>
            <?php endif; ?>
          </div>
        </div><!-- /.navbar-collapse -->
      </div> <!-- /.navbar-header__container -->
    </nav><!-- /.navbar -->
</header>
